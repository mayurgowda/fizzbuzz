public class Fizzbuzz {
    /*
  1
  2
  3
  4
  5
  ...
  100
  1
  2
  3 Fizz
  4
  5
  ...
  100
  1
  2
  3 Fizz
  4
  5 Buzz
  ...
  100 Buzz

*/
    public static void main(String[] args) {
        for (int i = 1; i < 101; i++) {
            p("" + i );
            if (i % 3 == 0){
                p(" Fizz");
            }
            if (i % 5 == 0){
                p(" Buzz");
            }

                pn("");
        }


    }

    public static void p(String s) {
        System.out.print(s);
    }

    public static void pn(String s) {
        System.out.println(s);
    }


}
